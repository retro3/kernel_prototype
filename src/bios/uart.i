;******************************************************************************
;*
;*  TX8 8-bit color computer
;*
;*  uart.i
;*    UART driver API
;*
;******************************************************************************

  .import   uart_init
  .import   uart_get_char
  .import   uart_put_char
