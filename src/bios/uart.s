;******************************************************************************
;*
;*  TX8 8-bit color computer
;*
;*  uart.s
;*    Low level uart driver for the SCC2681/82 uart chip
;*
;******************************************************************************
  .setcpu   "65c02"

  .export   uart_init
  .export   uart_get_char
  .export   uart_put_char

; Uart definitions
uart_base       =   $8400           ;Reliant hardware uart

serial_mr1a     =   uart_base       ;Mode register 1 A
serial_mr2a     =   uart_base       ;Mode register 2 A
serial_csra     =   uart_base+1     ;Clock select register
serial_cra      =   uart_base+2     ;Control register A
serial_acr      =   uart_base+4     ;Auxillary control register
serial_txdata   =   uart_base+3     ;ACIA WRITE DATA
serial_rxdata   =   uart_base+3     ;ACIA READ DATA
serial_status   =   uart_base+1     ;ACIA STATUS
serial_wcmd     =   uart_base+2     ;ACIA COMMAND REGISTER
serial_wctl     =   uart_base+3     ;ACIA CONTROL REGISTER
rxrdy           =   $01             ;RECEIVE READY
txrdy           =   $04             ;TRANSMIT READY

;******************************************************************************
;* Initialize the UART
;*  ACC -
;*    X -
;*    Y -
;* Implementation notes
;******************************************************************************
uart_init:
  lda   #$1a            ; Reset MR register pointer
  sta   serial_cra      ; and disable Rx and Tx
  lda   #$30
  sta   serial_cra      ; Reset transmitter
  lda   #$20
  sta   serial_cra      ; Reset receiver
  lda   #$13
  sta   serial_mr1a     ; No parity, 8 bits
  lda   #$07
  sta   serial_mr2a     ; 1 stop bit
  lda   #$00
  sta   serial_acr      ; Select baud rate set 1
  lda   #$cc            ; 38400 baud
  sta   serial_csra
  lda   #$10
  sta   serial_cra      ; Reset MR pointer
  lda   serial_mr1a
  cmp   #$13
  beq   passed_uart_check
  ; failed, read back incorrect value. Not much to do here but if we
  ; loop continously it is easy to catch with a logic analyzer.
error_loop:
  bne   error_loop
passed_uart_check:
  lda   #$50
  sta   serial_cra      ; Reset delta break
  lda   #$30
  sta   serial_cra      ; Reset transmitter
  lda   #$20
  sta   serial_cra      ; Reset receiver
  lda   #$45
  sta   serial_cra      ; Clear errrors and enable rx-tx.

  rts

;******************************************************************************
;* Non blocking get character function
;*  ACC - Returned character from UART
;*    X -
;*    Y -
;* Implementation notes
;******************************************************************************
uart_get_char:
  lda     serial_status
  and     #rxrdy
  beq     gc90            ;skip if no data yet
;
;  data received:  return cy=0. data in a
  sec                     ;data is available
  lda     serial_rxdata   ;read data
  rts
;
;  timeout:  return cy=1
gc90:
  clc                     ;no data yet
  rts

;******************************************************************************
;* Sends a character on the UART
;*  ACC - Holds the character to send
;*    X -
;*    Y -
;* Implementation notes
;******************************************************************************
uart_put_char:
        pha
upc_010:
        lda     serial_status   ;check tx status
        and     #txrdy          ;rx ready ?
        beq     upc_010
        pla
        sta     serial_txdata   ;transmit char.
        rts
