;******************************************************************************
;*
;*  TX8 8-bit color computer
;*
;*  macros.i
;*    Assortment of macros for the TX8 computer
;*
;******************************************************************************

;******************************************************************************
;* Load pointer to destination
;*  Usage: ldp  <ptr address>, <memory location>
;******************************************************************************
.macro  ldp ptr, m_ptr
  lda   #<m_ptr
  sta   ptr
  lda   #>m_ptr
  sta   ptr+1
.endmacro

lcount .set 0          ; Initialize the counter

.macro print m_ptr, x1, x2, x3, x4
  .segment "RODATA"
  .ident (.sprintf ("string_%04X", lcount)):
  .byte m_ptr
  .ifnblank x1
    .byte x1
  .endif
  .ifnblank x2
    .byte x2
  .endif
  .ifnblank x3
    .byte x3
  .endif
  .ifnblank x4
    .byte x4
  .endif
  .byte $00
  .segment "CODE"
  lda   #<.ident(.sprintf ("string_%04X", lcount))
  sta   l_ptr
  lda   #>.ident(.sprintf ("string_%04X", lcount))
  sta   l_ptr+1
  jsr   print_0
  lcount  .set lcount+1
.endmacro

.macro  load_sect lba_ptr
  lda   lba_ptr
  sta   cflba0
  lda   lba_ptr+1
  sta   cflba1
  lda   lba_ptr+2
  sta   cflba2
  lda   lba_ptr+3
  sta   cflba3
.endmacro
