;******************************************************************************
;*
;*  TX8 8-bit color computer
;*
;*  shell.s
;*    Basic command shell for the TX8 color computer.
;*
;*    This is the basic command interpreter for the computer.
;*
;******************************************************************************
  .setcpu     "65c02"

  .include    "cf_driver.i"
  .include    "macros.i"
  .include    "txfs.i"
  .include    "uart.i"

  .zeropage
l_ptr:      .res    2

  .data
d_size:     .res    4
gthan:      .res    2
tmp:        .res    2
reg32:      .res    4

bios_put_char = uart_put_char
bios_get_char = uart_get_char

  .segment  "RODATA"
prt_crlf:
  .byte     $0D,$0A,$00
str_kb:
  .byte     "KB", $00
str_mb:
  .byte     "MB", $00
prt_line:
  .byte "----------------------------------------", $00

  .segment  "CODE"
;******************************************************************************
;* Shell
;******************************************************************************
start:
  ; This first section should be a part of the BIOS but isn't available yet.
  lda   #0                  ; Mode 0 (= text mode)
;  jsr   gr_init_screen      ; Set text mode
;  jsr   gr_cls              ; Clear the screen
;  jsr   kbd_init            ; Initialize keyboard
;  jsr   bios_init           ; Initialize bios (only if standalone)
  jsr   cf_init             ; Initialize compact flash drive

  print "TX8 Color Computer", $0d, $0a

  ldp   dat_ptr, blkdat     ; setup disk buffer
  jsr   cf_info             ; Check if we can find a CF card
  bcc   found_cf_card       ; Carry is set if no card was found
  jmp   no_cf_card
found_cf_card:
  print "Found CF disk: "
  ; Store the size sector locally
  ldy   #120
  lda   (dat_ptr),y
  sta   d_size+0
  iny
  lda   (dat_ptr),y
  sta   d_size+1
  iny
  lda   (dat_ptr),y
  sta   d_size+2
  iny
  lda   (dat_ptr),y
  sta   d_size+3
  iny

  lda   d_size+2            ; Get LBA bits 16 - 23
  cmp   #7                  ; 256 MB ?
  bne   not_256mb
  print "256MB"
  bra   cont_001
not_256mb:
  cmp   #$0f                ; 512 MB ?
  bne   not_512mb
  print "512MB"
  bra   cont_001
not_512mb:
  cmp   #$1f                ; 1 GB ?
  bne   not_1gb
  print "1GB"
  bra   cont_001
not_1gb:
  cmp   #$3d                ; 2 GB ?
  bne   cont_001
  print "2GB"
cont_001:
  jsr   print_crlf

  print "RAM size: 128Kbyte", $0d, $0a
  print "Booting the system...", $0d, $0a

  ldp   dat_ptr, blkdat     ; setup disk buffer
  jsr   read_boot_sector    ; Get the super block

  ldp   dat_ptr, blkdat     ; Restore disk ptr
  ldy   #0
  lda   (dat_ptr),y
  cmp   #'S'                ; Check superblock ID
  bne   invalid_sb
  iny
  lda   (dat_ptr),y
  cmp   #'B'
  bne   invalid_sb

loopit:
  jsr   bios_get_char       ; Get keyboard stroke
  bcc   loopit

  cmp   #'S'                ; Write super block
  bne   not_P
  jsr   write_super_block
not_P:
  cmp   #'V'                ; View superblock information
  bne   not_V
  jsr   view_super_block
not_V:
  cmp   #'F'
  bne   not_F
  jsr   format_partition
not_F:
  jmp   loopit

no_cf_card:
  print "Could not detect a valid CF card"
  bra   loopit
invalid_sb:
  print "Invalid superblock found !"
  bra   loopit

str_disk_name:
  .byte     "DEV DISK", $00
;******************************************************************************
;* Write the superblock
;*   Populate the superblock with a known working superblock configuration
;******************************************************************************
write_super_block:
  ; First setup and clear the disk buffer
  ldp   dat_ptr, blkdat
  ldy   #0
  tya
  ldx   #2
wsb_001:
  sta   (dat_ptr),y
  iny
  bne   wsb_001
  inc   dat_ptr+1
  dex
  bne   wsb_001

  ; Block is now cleared so get some data there
  ldp   dat_ptr, blkdat     ; Restore data pointer
  ldy   #1
  lda   #'S'                ; ID 1
  sta   (dat_ptr)
  lda   #'B'                ; ID 2
  sta   (dat_ptr),y
  iny                       ; 2

  lda   #$01                ; Version
  sta   (dat_ptr),y
  iny                       ; 3

  lda   #$02
  sta   (dat_ptr),y         ; Number of active partitions
  iny                       ; 4

  ; Get disk size and subtract 1 to get the last sector
  sec
  lda   d_size+0
  sbc   #1
  sta   (dat_ptr),y
  iny                       ; 5

  lda   d_size+1
  sbc   #0
  sta   (dat_ptr),y
  iny                       ; 6

  lda   d_size+2
  sbc   #0
  sta   (dat_ptr),y
  iny                       ; 7

  lda   d_size+3
  sbc   #0
  sta   (dat_ptr),y
  iny                       ; 8

  ; copy disk name to buffer
  ldx   #0
wsb_002:
  lda   str_disk_name,x     ; Get predefined name
  sta   (dat_ptr),y
  iny
  inx
  cpx   #8                  ; Max 8 characters
  bne   wsb_002

  ; Partition 0
  ldy   #$10
  lda   #1
  sta   (dat_ptr),y

  ldy   #$14
  lda   #$ff
  sta   (dat_ptr),y
  iny
  sta   (dat_ptr),y

  ; Partition 1
  ldy   #$18
  lda   #1
  sta   (dat_ptr),y
  ldy   #$1a
  sta   (dat_ptr),y

  ldy   #$1c
  lda   #$ff
  sta   (dat_ptr),y
  iny
  sta   (dat_ptr),y

  jmp   write_boot_sector

;******************************************************************************
;* View the superblock
;*   Print the content of the superblock
;******************************************************************************
view_super_block:
  ldp   dat_ptr, blkdat     ; setup disk buffer
  jsr   read_boot_sector    ; Get the super block

  jsr   print_crlf
  print "Disk Label: "
  ldp   dat_ptr, blkdat     ; Reset disk buffer pointer
  ldy   #$08
vsb_001:
  lda   (dat_ptr),y
  beq   vsb_002
  jsr   bios_put_char
  iny
  cpy   #$10
  bne   vsb_001
vsb_002:
  jsr   print_crlf

  print "Number of partitions: "
  ldy   #3
  lda   (dat_ptr),y         ; Get number of partitions
  sta   tmp
  sta   l_ptr
  stz   l_ptr+1
  ldx   #l_ptr
  jsr   print_dec16         ; Print it
  jsr   print_crlf

  print "Partition list:", $0d, $0a
  print " #  First    Last      Size  Status", $0d, $0a
  ldp   l_ptr, prt_line
  jsr   print_0               ; Print list header

  ; Adjust datapointer to base of partition list
  lda   dat_ptr
  clc
  adc   #part_offset
  sta   dat_ptr
  lda   dat_ptr+1
  adc   #0
  sta   dat_ptr+1
  ldx   #0
vsb_003:
  cpx   #10
  bcs   vsb_004
  lda   #' '                ; Right adjustment
  jsr   bios_put_char
vsb_004:
  phx
  stx   l_ptr
  stz   l_ptr+1
  ldx   #l_ptr
  jsr   print_dec16         ; Print partition #

  jsr   print_double_space
  jsr   print_start_sector  ; Print partition start and end sector
  jsr   print_double_space
  jsr   print_part_size     ; Print partition size
  jsr   print_double_space
  jsr   print_status_bits

  jsr   print_crlf          ; New line

  lda   dat_ptr             ; Get next partition information entry
  clc
  adc   #txfs_pi_size
  sta   dat_ptr
  lda   dat_ptr+1
  adc   #0
  sta   dat_ptr+1

  plx
  inx
  cpx   tmp
  bne   vsb_003

  ldp   l_ptr, prt_line
  jsr   print_0               ; Print list header

  rts

;******************************************************************************
;* Print status bits
;*   Prints the status bits of this partition
;******************************************************************************
print_status_bits:
  ldy   #16
  lda   (dat_ptr),y         ; Get status
  ldy   #8
pds_001:
  asl   a
  pha
  bcs   pds_1
  lda   #'0'
  bra   pds_002
pds_1:
  lda   #'1'
pds_002:
  jsr   bios_put_char
  pla
  dey
  bne   pds_001

  rts

;******************************************************************************
;* Print partition size.
;*   Prints the partition size.
;*   Partitions smaller than 1 MByte are printed as KB and larger as MB
;******************************************************************************
print_double_space:
  lda   #' '
  jsr   bios_put_char
  jmp   bios_put_char

;******************************************************************************
;* Print partition size.
;*   Prints the partition size.
;*   Partitions smaller than 1 MByte are printed as KB and larger as MB
;******************************************************************************
print_part_size:
  ldy   #5
  lda   (dat_ptr),y         ; Load high byte.
  cmp   #8
  bcs   pps_do_MB
  ; Print as kilobyte
  sta   reg32+1
  dey
  lda   (dat_ptr),y
  sta   reg32

  lsr   reg32
  ror   reg32+1
  ror   reg32+2
  ror   reg32+3             ; Dividing the number of sectors by 2 gives KB

  lda   reg32
  sta   l_ptr
  lda   reg32+1
  sta   l_ptr+1

  ldx   #l_ptr
  jsr   print_dec16
  ldp   l_ptr, str_kb
  jmp   print_0
pps_do_MB:
  sta   l_ptr               ; This is equal to dividing the number of
  stz   l_ptr+1             ; sectors by 2048 which gives MB

  lsr   l_ptr
  lsr   l_ptr
  lsr   l_ptr

  ldx   #l_ptr
  jsr   print_dec16
  ldp   l_ptr, str_mb
  jmp   print_0

;******************************************************************************
;* Print start sector
;*   Prints the 32 bit LBA number pointer to by dat_ptr
;******************************************************************************
print_start_sector:
  ldy   #3
  lda   (dat_ptr),y
  sta   reg32+3
  jsr   print_hex8
  dey

  lda   (dat_ptr),y
  sta   reg32+2
  jsr   print_hex8
  dey

  lda   (dat_ptr),y
  sta   reg32+1
  jsr   print_hex8
  dey

  lda   (dat_ptr),y
  sta   reg32+0
  jsr   print_hex8

  lda   #'-'
  jsr   bios_put_char
  ; Add size to start sector
  ldy   #4
  lda   (dat_ptr),y
  clc
  adc   reg32+0
  sta   reg32+0
  iny
  lda   (dat_ptr),y
  adc   reg32+1
  sta   reg32+1
  lda   reg32+2
  adc   #0
  sta   reg32+2
  lda   reg32+3
  adc   #0
  sta   reg32+3
  ; Print last sector in partition
  lda   reg32+3
  jsr   print_hex8
  lda   reg32+2
  jsr   print_hex8
  lda   reg32+1
  jsr   print_hex8
  lda   reg32+0
  jmp   print_hex8

part_dat:
  .byte   "TXFS"        ; Magic number for the file system (TXFS)
  .byte   $01           ; Version of file system
  .dword  $00000001     ; The first sector of this filesystem
  .dword  $00010000     ; The last sector of the filesystem
  .word   $ABEF         ; Numeric ID of the partition
  .byte   "Games disk", $00, $00, $00, $00, $00, $00, $00
  .byte   $00, $00, $00, $00, $00, $00, $00, $00
  .byte   $00, $00, $00, $00, $00, $00, $00, $00 ; Text description of partition
  .dword  $00000002     ; First logical block of inode bitmap
  .byte   $01           ; Length of inode bitmap (in blocks)
  .dword  $00000003     ; First logical block of data bitmap
  .byte   $10           ; Length of data bitmap (in blocks)
  .dword  $00000013     ; First logical block of inode region
  .word   1024          ; Length of inode region (in blocks)
  .dword  $00000413     ; First logical block of user data region
  .word   64493         ; Length of user data region (in blocks)
  .word   $a0be         ; Status bits

;******************************************************************************
;******************************************************************************
;*
;* Format a partition
;*  This first test will read fixed data from a table to create a filsystem
;*  for a pre determined partition size. Length = 65535 sectors
;*
;******************************************************************************
;******************************************************************************
format_partition:
  lda   #0
  jsr   clear_sector_buffer
  print "Formatting the partition."

  ; Transfer the filesystem configuration
  ldp   l_ptr, part_dat
  ldy   #0
fp_001:
  lda   (l_ptr),y
  sta   (dat_ptr),y         ; Setup disk buffer
  iny
  cpy   #(txfs_partition::status + 2)
  bne   fp_001

  lda   part_dat + txfs_partition::first_sect
  sta   cflba0
  lda   part_dat + txfs_partition::first_sect+1
  sta   cflba1
  lda   part_dat + txfs_partition::first_sect+2
  sta   cflba2
  lda   part_dat + txfs_partition::first_sect+3
  sta   cflba3

;  jsr   cf_write_sector

  ; Now make sure all bits in the Inode bitmap are cleared.
  ; A free entry is marked with a 0 and an occupied entry is 1.
  lda   #$ff
  jsr   clear_sector_buffer
  rts

;******************************************************************************
;* Clear data region
;*   Clear the entire sector buffer and returns with the dat_ptr restored.
;*   ACC holds the data that should be written to the buffer.
;******************************************************************************
clear_sector_buffer:
  pha
  ldp dat_ptr, blkdat
  ldy   #0
  pla
csb_001:
  sta   (dat_ptr), y
  dey
  bne   csb_001
  inc   dat_ptr+1
csb_002:
  sta   (dat_ptr), y
  dey
  bne   csb_002
  dec   dat_ptr+1
  rts

;******************************************************************************
;* Read boot sector
;*   Simply reads the first sector of the disk.
;******************************************************************************
read_boot_sector:
  lda   #0
  sta   cflba3
  sta   cflba2
  sta   cflba1
  sta   cflba0
  jsr   cf_read_sector

  rts

;******************************************************************************
;* Write boot sector
;*   Simply writes the first sector of the disk.
;******************************************************************************
write_boot_sector:
  lda   #0
  sta   cflba3
  sta   cflba2
  sta   cflba1
  sta   cflba0
  jsr   cf_write_sector

  rts

;******************************************************************************
;* Print 8 bit hex value.
;* Parameter:
;*  ACC - 8 bit input value
;******************************************************************************
print_hex8:
  pha
  lsr
  lsr
  lsr
  lsr
  cmp   #10
  bcc   ph_001
  adc   #6
ph_001:
  adc   #$30
  jsr   bios_put_char

  pla
  and   #$0f
  cmp   #10
  bcc   ph_002
  adc   #6
ph_002:
  adc   #$30
  jmp   bios_put_char

;******************************************************************************
;* Print 16 bit hex value
;* Parameter:
;*  ACC - high byte of 16 bit word
;*  X   - low byte of input value
;******************************************************************************
print_hex16:
  jsr   print_hex8
  txa
  jmp   print_hex8

;******************************************************************************
;* Print 16 bit decimal value
;* Parameter:
;*  X   - points to the 16bit variable to print
;******************************************************************************
print_dec16:
  lda   #0                  ; null delimiter for print
  pha
prnum2:                     ;   divide var[x] by 10
  lda   #0
  sta   gthan+1             ; clr BCD
  lda   #16
  sta   gthan               ; {>} = loop counter
prdiv1:
  asl   0,x                 ; var[x] is gradually replaced
  rol   1,x                 ;   with the quotient
  rol   gthan+1             ; BCD result is gradually replaced
  lda   gthan+1             ;   with the remainder
  sec
  sbc   #10                 ; partial BCD >= 10 ?
  bcc   prdiv2
  sta   gthan+1             ;   yes: update the partial result
  inc   0,x                 ;   set low bit in partial quotient
prdiv2:
  dec   gthan
  bne   prdiv1              ; loop 16 times
  lda   gthan+1
  ora   #'0'                ;   convert BCD result to ASCII
  pha                       ;   stack digits in ascending
  lda   0,x                 ;     order ('0' for zero)
  ora   1,x
  bne   prnum2              ; } until var[x] is 0
  pla
prnum3:
  jsr   bios_put_char         ; print digits in descending
  pla                       ;   order until delimiter is
  bne   prnum3              ;   encountered
  rts

;******************************************************************************
;* Print string at l_ptr.
;*   Does not support more than 256 characters and is zero terminated
;******************************************************************************
print_0:
  ldy   #0
print_001:
  lda   (l_ptr),y             ; Get character
  beq   print_end             ; End of string ?
  jsr   bios_put_char           ; Print character
  iny
  bne   print_001
print_end:
  rts

;******************************************************************************
;* Print carriage return and line feed
;******************************************************************************
print_crlf:
  ldp   l_ptr, prt_crlf
  jmp   print_0

  .segment  "DOSDATA"
blkdat:   .res    512
