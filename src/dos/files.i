;******************************************************************************
;*
;*  TX8 8-bit color computer
;*
;*  files.i
;*    Include file containing usefull macros and data structures used in and
;*    needed to perform function calls from external programs.
;*
;******************************************************************************

;******************************************************************************
;* macros
;******************************************************************************
.macro  ldp   string
        lda   #<string
        sta   str_ptr
        lda   #>string
        sta   str_ptr+1
.endmacro
