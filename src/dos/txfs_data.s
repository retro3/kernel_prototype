;******************************************************************************
;*
;*  TX8 8-bit color computer
;*
;*  txfs_data.s
;*    Global data areas for the file system
;*
;******************************************************************************
.include    "txfs.i"

.export     part_info
.export     part_sect
.export     part_info_len
.export     byte_bits

.segment    "DATA"

; TODO: We need one instance for each partition that is mounted
part_info:  .tag  txfs_partition
part_sect:  .tag  txfs_pi

byte_bits:  .byte $80, $40, $20, $10, $08, $04, $02, $01

part_info_len = .sizeof(part_info)
