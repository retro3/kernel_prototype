PROGRAM = ehBasic
CC      := cc65
AS			:= ca65
LD			:= ld65
RM			:= rm -f
SYM			:= python3 makenoi.py
S_EXT   := %.s
O_EXT   := %.o

MODULES   := os dos bios
#SRC_DIR   := $(MODULES)
SRC_DIR   := $(addprefix src/,$(MODULES))
BUILD_DIR := $(addprefix build/,$(MODULES))

SRC       := $(foreach sdir,$(SRC_DIR),$(wildcard $(sdir)/*.s))
OBJ       := $(patsubst src/%.s,build/%.o,$(SRC))
DEP 			:= $(OBJ:%.o=%.o.d)
INCLUDES  := $(addprefix -I,$(SRC_DIR))

LDFLAGS := -Ln build/$(PROGRAM).vic -C tx8.cfg -m build/$(PROGRAM).map
ASFLAGS := -g $(INCLUDES) -Isrc/util

vpath %.s $(SRC_DIR)

define make-goal
$1/%.o: %.s
	$(AS) $(ASFLAGS) $$< -o $$@ --create-dep $$@.d -l $$@.lst
endef

.PHONY: all checkdirs clean
	@echo $(SRC_DIR)

all: checkdirs $(PROGRAM)

$(PROGRAM): $(OBJ)
	$(LD) $(LDFLAGS) $^ -o build/$(PROGRAM).bin
	$(SYM) build/$(PROGRAM).vic >build/$(PROGRAM).sym		# Convert vice tables to noice format
	@grep -A 8 'Segment' build/ehBasic.map

checkdirs: $(BUILD_DIR)

$(BUILD_DIR):
	@mkdir -p $@

-include $(DEP)		# Make sure all dependencies are handled

$(foreach bdir,$(BUILD_DIR),$(eval $(call make-goal,$(bdir))))

clean:
	@rm -rf $(BUILD_DIR)
	@rm -rf build

test:
	@echo $(DEP)
